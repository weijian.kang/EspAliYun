#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_err.h"
#include "esp_event.h"
#include "esp_log.h"

#include "infra_compat.h"
#include "rgb_led.h"
#include "button.h"
#include "mqtt_solo.h"
#include "conn_mgr.h"

#define EXAMPLE_WIFI_SSID "小康师兄"
#define EXAMPLE_WIFI_PASS "12345678"

static const char* TAG = "app main";

static bool mqtt_started = false;
// static int led_switch, red, green, blue;

static esp_err_t wifi_event_handle(void *ctx, system_event_t *event)
{
    switch (event->event_id) {
        case SYSTEM_EVENT_STA_GOT_IP:
            if (mqtt_started == false) {
                xTaskCreate((void (*)(void *))mqtt_main, "mqtt_example", 10240, NULL, 5, NULL);
                mqtt_started = true;
            }
            break;
        default:
            break;
    }
    return ESP_OK;
}

void app_main()
{
    button_init();
    rgb_led_init();
    conn_mgr_init();
    conn_mgr_register_wifi_event(wifi_event_handle);
    conn_mgr_set_wifi_config_ext((const uint8_t *)EXAMPLE_WIFI_SSID, strlen(EXAMPLE_WIFI_SSID), (const uint8_t *)EXAMPLE_WIFI_PASS, strlen(EXAMPLE_WIFI_PASS));

    IOT_SetLogLevel(IOT_LOG_INFO);

    conn_mgr_start();
}

