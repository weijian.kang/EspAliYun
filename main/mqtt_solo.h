#ifndef _MQTT_SOLO_H__
#define _MQTT_SOLO_H__

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "mqtt_api.h"
#include "dm_wrapper.h"
#include "cJSON.h"
#include "rgb_led.h"

int example_publish(void);
int mqtt_main(void *paras);

#endif
