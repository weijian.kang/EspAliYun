#ifndef _RGB_LED_H__
#define _RGB_LED_H__

typedef struct {  
    int led_switch;
    int red, green, blue;
} rgb_led_t;

void rgb_led_init(void);
void rgb_led_update(void);
// void rgb_led_update(int red, int green, int blue);

#endif
