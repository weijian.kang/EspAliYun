#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "esp_log.h"
#include "driver/rmt.h"
#include "led_strip.h"
#include "rgb_led.h"


#define RMT_TX_CHANNEL RMT_CHANNEL_0
#define EXAMPLE_RMT_TX_GPIO 8
#define EXAMPLE_STRIP_LED_NUMBER 1

static const char *TAG = "rgb_led";

led_strip_t *strip;
rgb_led_t rgb_led;

void rgb_led_update(void)
{
    ESP_LOGI(TAG, "rgb_led_update led_switch: %d, red: %d, green: %d, blue: %d", rgb_led.led_switch, rgb_led.red, rgb_led.green, rgb_led.blue);
    if(rgb_led.led_switch){
        strip->set_pixel(strip, 0, rgb_led.red, rgb_led.green, rgb_led.blue);
    }else{
        strip->set_pixel(strip, 0, 0, 0, 0);
    }
    strip->refresh(strip, 50);
}

void rgb_led_init(void)
{
    rmt_config_t config = RMT_DEFAULT_CONFIG_TX(EXAMPLE_RMT_TX_GPIO, RMT_TX_CHANNEL);
    // set counter clock to 40MHz
    config.clk_div = 2;

    ESP_ERROR_CHECK(rmt_config(&config));
    ESP_ERROR_CHECK(rmt_driver_install(config.channel, 0, 0));

    // install ws2812 driver
    led_strip_config_t strip_config = LED_STRIP_DEFAULT_CONFIG(EXAMPLE_STRIP_LED_NUMBER, (led_strip_dev_t)config.channel);
    strip = led_strip_new_rmt_ws2812(&strip_config);
    if (!strip) {
        ESP_LOGE(TAG, "install WS2812 driver failed");
    }
    // Clear LED strip (turn off all LEDs)
    ESP_ERROR_CHECK(strip->clear(strip, 100));
    // Show simple rainbow chasing pattern
    ESP_LOGI(TAG, "LED Rainbow Chase Start");
}
