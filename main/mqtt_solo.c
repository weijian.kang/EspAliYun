#include "mqtt_solo.h"

char DEMO_PRODUCT_KEY[IOTX_PRODUCT_KEY_LEN + 1] = {0};
char DEMO_DEVICE_NAME[IOTX_DEVICE_NAME_LEN + 1] = {0};
char DEMO_DEVICE_SECRET[IOTX_DEVICE_SECRET_LEN + 1] = {0};

extern rgb_led_t rgb_led;

#define EXAMPLE_TRACE(fmt, ...)  \
    do { \
        HAL_Printf("%s|%03d :: ", __func__, __LINE__); \
        HAL_Printf(fmt, ##__VA_ARGS__); \
        HAL_Printf("%s", "\r\n"); \
    } while(0)

void example_message_arrive(void *pcontext, void *pclient, iotx_mqtt_event_msg_pt msg)
{
    cJSON * root = NULL;
    cJSON * params = NULL;
    cJSON * rgb_color = NULL;

    iotx_mqtt_topic_info_t     *topic_info = (iotx_mqtt_topic_info_pt) msg->msg;

    switch (msg->event_type) {
        case IOTX_MQTT_EVENT_PUBLISH_RECEIVED:
            /* print topic name and topic message */
            EXAMPLE_TRACE("Message Arrived:");
            EXAMPLE_TRACE("Topic  : %.*s", topic_info->topic_len, topic_info->ptopic);
            EXAMPLE_TRACE("Payload: %.*s", topic_info->payload_len, topic_info->payload);
            EXAMPLE_TRACE("\n");
            root = cJSON_Parse(topic_info->payload);     
            params = cJSON_GetObjectItem(root, "params");
            rgb_color = cJSON_GetObjectItem(params, "RGBColor");
            rgb_led.led_switch = (int)cJSON_GetNumberValue(cJSON_GetObjectItem(params, "LEDSwitch"));
            rgb_led.red = (int)cJSON_GetNumberValue(cJSON_GetObjectItem(rgb_color, "Red"));
            rgb_led.green = (int)cJSON_GetNumberValue(cJSON_GetObjectItem(rgb_color, "Green"));
            rgb_led.blue = (int)cJSON_GetNumberValue(cJSON_GetObjectItem(rgb_color, "Blue"));
            rgb_led_update();
            example_publish();
            break;
        default:
            break;
    }
}

int example_subscribe(void *handle)
{
    int res = 0;
    const char *fmt = "/sys/%s/%s/thing/service/property/set";
    char *topic = NULL;
    int topic_len = 0;

    topic_len = strlen(fmt) + strlen(DEMO_PRODUCT_KEY) + strlen(DEMO_DEVICE_NAME) + 1;
    topic = HAL_Malloc(topic_len);
    if (topic == NULL) {
        EXAMPLE_TRACE("memory not enough");
        return -1;
    }
    memset(topic, 0, topic_len);
    HAL_Snprintf(topic, topic_len, fmt, DEMO_PRODUCT_KEY, DEMO_DEVICE_NAME);

    res = IOT_MQTT_Subscribe(handle, topic, IOTX_MQTT_QOS0, example_message_arrive, NULL);
    if (res < 0) {
        EXAMPLE_TRACE("subscribe failed");
        HAL_Free(topic);
        return -1;
    }

    HAL_Free(topic);
    return 0;
}

int example_publish(void)
{
    int             res = 0;
    char           *topic = NULL;
    const char     *topic_fmt = "/sys/%s/%s/thing/event/property/post";
    int             topic_len = 0;
    char           *payload = NULL; 
    const char     *payload_fmt = "{ \
                        \"version\": \"1.0\", \
                        \"params\": { \
                            \"RGBColor\": { \
                                \"value\": %s, \
                            },\
                            \"LEDSwitch\": { \
                                \"value\": %d, \
                            }\
                        },\
                        \"method\": \"thing.event.property.post\"  \      
                    }";
    int             payload_len = 0;
    cJSON          *rgb =  cJSON_CreateObject();

    topic_len = strlen(topic_fmt) + strlen(DEMO_PRODUCT_KEY) + strlen(DEMO_DEVICE_NAME) + 1;
    topic = HAL_Malloc(topic_len);
    if (topic == NULL) {
        EXAMPLE_TRACE("memory not enough");
        return -1;
    }
    memset(topic, 0, topic_len);
    HAL_Snprintf(topic, topic_len, topic_fmt, DEMO_PRODUCT_KEY, DEMO_DEVICE_NAME);

    cJSON_AddItemToObject(rgb, "Red", cJSON_CreateNumber(rgb_led.red));
    cJSON_AddItemToObject(rgb, "Green", cJSON_CreateNumber(rgb_led.green));
    cJSON_AddItemToObject(rgb, "Blue", cJSON_CreateNumber(rgb_led.blue));    
    payload_len = strlen(payload_fmt) + strlen(cJSON_Print(rgb)) + 1 + 1;
    payload = HAL_Malloc(payload_len);
    if (payload == NULL) {
        EXAMPLE_TRACE("memory not enough");
        return -1;
    }
    memset(payload, 0, payload_len);
    HAL_Snprintf(payload, payload_len, payload_fmt, cJSON_Print(rgb), rgb_led.led_switch);
    EXAMPLE_TRACE("publish %s", payload);

    res = IOT_MQTT_Publish_Simple(0, topic, IOTX_MQTT_QOS0, payload, strlen(payload));
    if (res < 0) {
        EXAMPLE_TRACE("publish failed, res = %d", res);
        HAL_Free(topic);
        return -1;
    }

    HAL_Free(topic);
    return 0;
}

void example_event_handle(void *pcontext, void *pclient, iotx_mqtt_event_msg_pt msg)
{
    EXAMPLE_TRACE("msg->event_type : %d", msg->event_type);

    switch(msg->event_type)
    {
        case IOTX_MQTT_EVENT_UNDEF:
            EXAMPLE_TRACE("msg->event_type : %d, IOTX_MQTT_EVENT_UNDEF", msg->event_type);
            break;
        case IOTX_MQTT_EVENT_DISCONNECT:
            EXAMPLE_TRACE("msg->event_type : %d, IOTX_MQTT_EVENT_DISCONNECT", msg->event_type);
            break;
        case IOTX_MQTT_EVENT_RECONNECT:
            EXAMPLE_TRACE("msg->event_type : %d, IOTX_MQTT_EVENT_RECONNECT", msg->event_type);
            break;
        case IOTX_MQTT_EVENT_SUBCRIBE_SUCCESS:
            EXAMPLE_TRACE("msg->event_type : %d, IOTX_MQTT_EVENT_SUBCRIBE_SUCCESS", msg->event_type);
            break;
        case IOTX_MQTT_EVENT_SUBCRIBE_TIMEOUT:
            EXAMPLE_TRACE("msg->event_type : %d, IOTX_MQTT_EVENT_SUBCRIBE_TIMEOUT", msg->event_type);
            break;
        case IOTX_MQTT_EVENT_SUBCRIBE_NACK:
            EXAMPLE_TRACE("msg->event_type : %d, IOTX_MQTT_EVENT_SUBCRIBE_NACK", msg->event_type);
            break;
        case IOTX_MQTT_EVENT_UNSUBCRIBE_SUCCESS:
            EXAMPLE_TRACE("msg->event_type : %d, IOTX_MQTT_EVENT_UNSUBCRIBE_SUCCESS", msg->event_type);
            break;
        case IOTX_MQTT_EVENT_UNSUBCRIBE_TIMEOUT:
            EXAMPLE_TRACE("msg->event_type : %d, IOTX_MQTT_EVENT_UNSUBCRIBE_TIMEOUT", msg->event_type);
            break;
        case IOTX_MQTT_EVENT_UNSUBCRIBE_NACK:
            EXAMPLE_TRACE("msg->event_type : %d, IOTX_MQTT_EVENT_UNSUBCRIBE_NACK", msg->event_type);
            break;
        case IOTX_MQTT_EVENT_PUBLISH_SUCCESS:
            EXAMPLE_TRACE("msg->event_type : %d, IOTX_MQTT_EVENT_PUBLISH_SUCCESS", msg->event_type);
            break;
        case IOTX_MQTT_EVENT_PUBLISH_TIMEOUT:
            EXAMPLE_TRACE("msg->event_type : %d, IOTX_MQTT_EVENT_PUBLISH_TIMEOUT", msg->event_type);
            break;
        case IOTX_MQTT_EVENT_PUBLISH_NACK:
            EXAMPLE_TRACE("msg->event_type : %d, IOTX_MQTT_EVENT_PUBLISH_NACK", msg->event_type);
            break;
        case IOTX_MQTT_EVENT_PUBLISH_RECEIVED:
            EXAMPLE_TRACE("msg->event_type : %d, IOTX_MQTT_EVENT_PUBLISH_RECEIVED", msg->event_type);
            break;
        case IOTX_MQTT_EVENT_BUFFER_OVERFLOW:
            EXAMPLE_TRACE("msg->event_type : %d, IOTX_MQTT_EVENT_BUFFER_OVERFLOW", msg->event_type);
            break;
    }
}

int mqtt_main(void *paras)
{
    void                   *pclient = NULL;
    int                     res = 0;
    iotx_mqtt_param_t       mqtt_params;

    HAL_GetProductKey(DEMO_PRODUCT_KEY);
    HAL_GetDeviceName(DEMO_DEVICE_NAME);
    HAL_GetDeviceSecret(DEMO_DEVICE_SECRET);

    EXAMPLE_TRACE("mqtt example");

    /* Initialize MQTT parameter */
    /*
     * Note:
     *
     * If you did NOT set value for members of mqtt_params, SDK will use their default values
     * If you wish to customize some parameter, just un-comment value assigning expressions below
     *
     **/
    memset(&mqtt_params, 0x0, sizeof(mqtt_params));

    /**
     *
     *  MQTT connect hostname string
     *
     *  MQTT server's hostname can be customized here
     *
     *  default value is ${productKey}.iot-as-mqtt.cn-shanghai.aliyuncs.com
     */
    /* mqtt_params.host = "something.iot-as-mqtt.cn-shanghai.aliyuncs.com"; */

    /**
     *
     *  MQTT connect port number
     *
     *  TCP/TLS port which can be 443 or 1883 or 80 or etc, you can customize it here
     *
     *  default value is 1883 in TCP case, and 443 in TLS case
     */
    /* mqtt_params.port = 1883; */

    /**
     *
     * MQTT request timeout interval
     *
     * MQTT message request timeout for waiting ACK in MQTT Protocol
     *
     * default value is 2000ms.
     */
    /* mqtt_params.request_timeout_ms = 2000; */

    /**
     *
     * MQTT clean session flag
     *
     * If CleanSession is set to 0, the Server MUST resume communications with the Client based on state from
     * the current Session (as identified by the Client identifier).
     *
     * If CleanSession is set to 1, the Client and Server MUST discard any previous Session and Start a new one.
     *
     * default value is 0.
     */
    /* mqtt_params.clean_session = 0; */

    /**
     *
     * MQTT keepAlive interval
     *
     * KeepAlive is the maximum time interval that is permitted to elapse between the point at which
     * the Client finishes transmitting one Control Packet and the point it starts sending the next.
     *
     * default value is 60000.
     */
    /* mqtt_params.keepalive_interval_ms = 60000; */

    /**
     *
     * MQTT write buffer size
     *
     * Write buffer is allocated to place upstream MQTT messages, MQTT client will be limitted
     * to send packet no longer than this to Cloud
     *
     * default value is 1024.
     *
     */
    /* mqtt_params.write_buf_size = 1024; */

    /**
     *
     * MQTT read buffer size
     *
     * Write buffer is allocated to place downstream MQTT messages, MQTT client will be limitted
     * to recv packet no longer than this from Cloud
     *
     * default value is 1024.
     *
     */
    /* mqtt_params.read_buf_size = 1024; */

    /**
     *
     * MQTT event callback function
     *
     * Event callback function will be called by SDK when it want to notify user what is happening inside itself
     *
     * default value is NULL, which means PUB/SUB event won't be exposed.
     *
     */
    mqtt_params.handle_event.h_fp = example_event_handle;

    pclient = IOT_MQTT_Construct(&mqtt_params);
    if (NULL == pclient) {
        EXAMPLE_TRACE("MQTT construct failed");
        return -1;
    }

    res = example_subscribe(pclient);
    if (res < 0) {
        IOT_MQTT_Destroy(&pclient);
        return -1;
    }

    example_publish();
    while (1) {
        IOT_MQTT_Yield(pclient, 1000);
    }

    return 0;
}

